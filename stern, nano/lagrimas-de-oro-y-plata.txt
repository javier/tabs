Dsus2               E-97th Csus2           Bm(5#)
Cuentan los viejos abuelos que muchos años atrás
Gm/Bb               D   G/D              G   A
el sol no aguantaba más a su mujer en el cielo
Bm(5#)               A/C#  A7           D
y que en medio de su vuelo a la luna le pegó
G       Aadd11/C#   Em - D  E-9       Bm(5#)       A 
con mucha fuerza le dio    golpes que también lo hirieron
G       A      (F#-E)   A                     Dsus2
y tanto dolor le dieron que después se arrepintió
   
Dsus2               E-97th Csus2           Bm(5#)
La luna que estaba llena, luego se puso a llorar
Gm/Bb               D   G/D              G   A
y comenzó a derramar lágrimas de tanta pena
Bm(5#)               A/C#  A7           D
y en una noche serena todo se empezó a cubrir
G       Aadd11/C#   Em - D  E-9       Bm(5#)       A 
la tierra se fue a cubrir de esos copitos plateados
      A      (F#-E)   A                     Dsus2
que habían sido derramados para dejar de sufrir

Al verla el sol se dio cuenta de su tremendo dolor

y así sintió el amargor de su pasión tan violenta

entre su pena verguenza ya no se pudo aguantar

también se puso a llorar lágrimas que eran de oro

y luego con su tesoro toda la tierra y el mar

y luego con su tesoro toda la tierra y el mar