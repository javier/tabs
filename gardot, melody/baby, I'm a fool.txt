G6    F6    F#6   G6    (x2)

G6                              Gadd(b13)                 Am7         D9
How was I to know that this was always only just a little game to you?
Am7                               D9                                  
All the time I felt you gave your heart I thought that I would do the 
G6
same for you,
Dm7                                  G7                    Cmaj7
Tell the truth I think I should have seen it coming from a mile away,
         Cdis
When the words you say are,
G6                              Cm6     Cdis    G6    F6    F#6
Baby I'm a fool who thinks it's cool to fall in love?

G6                          Gadd(b13)                     Am7           D9
If I gave a thought to fascination I would know it wasn't right to care,
Am7                             D9                     G6
Logic doesn't seem to mind that I am fascinated by the love affair,
Dm7                                  G7                    Cmaj7             
Still my heart would benefit from a little tenderness from time to time,
Cdis
but never mind,
      G6                              Cm6     Cdis    G6    F6    F#6
Cause baby I'm a fool who thinks it's cool to fall in love

G6                           Gadd(b13)                       Am7      D9
Baby I should hold on just a moment and be sure it's not for vanity,
Am7                            D9                                  G6
Look me in the eye and tell me love is never based upon insanity,
Dm7                                  G7                    Cmaj7
Even when my heart is beating hurry up the moment's fleeting,
Cmaj7
Kiss me now,
      Cdis
Don't ask me home
      G6                              Cm6     Cdis
Cause baby I'm a fool who thinks it's cool to fall,
G6                              Cm6     Cdis
Baby I'm a fool who thinks it's cool to fall,
      G6                      F6                F#6     G6    F6    F#6   G6 
And I would never tell if you became a fool and fell in love.